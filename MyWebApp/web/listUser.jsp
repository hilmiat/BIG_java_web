<%-- 
    Document   : listUser
    Created on : Sep 7, 2016, 2:02:34 PM
    Author     : my
--%>

<%@page import="java.util.List"%>
<%@page import="model.User"%>
<%@page import="java.util.ArrayList"%>

<a href="user?action=add">
    <span class="glyphicon glyphicon-plus"></span>Add User
</a>
<table class="table table-striped">
    <thead>
        <tr>
            <td>No</td>
            <td>Username</td>
            <td>Email</td>
            <td>Address</td>
            <td>Age</td>
            <td>Action</td>
        </tr>
    </thead>
    <% 
        ArrayList<User> data;
        data = (ArrayList<User>)request.getAttribute("datauser");
        int no = 0;
        for(User u :data){
            no = no + 1;         
    %>
        <tr>
            <td><%=no%></td>
            <td><%=u.getUsername()%></td>
            <td><%=u.getEmail()%></td>
            <td><%=u.getAddress()%></td>
            <td><%=u.getAge()%></td>
            <td>
                <a href="user?id=<%=u.getId()%>">
                    <span class="glyphicon glyphicon-eye-open"></span>
                </a>
                <!--icon & link untuk hapus-->
                <a href="user?delete=<%=u.getId()%>">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
                <!--icon & link untuk update-->
                <a href="user?action=edit&idedit=<%=u.getId()%>">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </td>
        </tr>
    <% } %>
</table>