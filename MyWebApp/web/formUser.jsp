<%-- 
    Document   : formUser
    Created on : Sep 8, 2016, 10:16:04 AM
    Author     : my
--%>
<jsp:useBean id="user" scope="request" class="model.User" />
<jsp:setProperty name="user" property="*"/>
<%
    if(user.getId()>0){
%>
        <h1>Update User</h1>   
<%  }else{ %>
        <h1>Registrasi User</h1>
<%  } %>
<form action="user" method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="username" 
                   value="${user.getUsername()}" placeholder="username"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input class="form-control" type="email" name="email" 
                   value="${user.getEmail()}" placeholder="email"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Age</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="age" 
                   value="${user.getAge()}" placeholder="age"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Address</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="address"
                      placeholder="address">${user.getAddress()}</textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <%
                int id = (int)user.getId();
                if(id>0){
            %>   
                <input type="submit" class="btn btn-info" value="Update"/>
                <input type="hidden" name="idedit" value="${user.getId()}" />
            <%
                }else{
            %>
                    <input type="submit" class="btn btn-info" value="Register"/>
            <% } %>
            
        </div>
    </div>
</form>















