<%-- 
    Document   : viewUser
    Created on : Sep 7, 2016, 3:32:52 PM
    Author     : my
--%>
<%@page import="model.User"%>
<h1>Profil user</h1>

<table class="table table-bordered table-striped">
    <tr>
        <td>Nama</td>
        <td><%=((User)request.getAttribute("user")).getUsername()%></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><%=((User)request.getAttribute("user")).getEmail()%></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><%=((User)request.getAttribute("user")).getAddress()%></td>
    </tr>
    <tr>
        <td>Age</td>
        <td><%=((User)request.getAttribute("user")).getAge()%></td>
    </tr>
</table>