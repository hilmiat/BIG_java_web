<%-- 
    Document   : index.
    Created on : Sep 7, 2016, 10:21:44 AM
    Author     : my
--%>
<jsp:include page="header.jsp" flush="true"/>

<!--konten halaman index-->
<div class="container">
<!--  <h1>Ini Halaman index</h1> -->
<%
    String hal = (String)request.getAttribute("hal");
    hal=(hal!=null)?hal:"error.jsp";
%>
    <jsp:include page="<%=hal%>" flush="true"/>
</div>

<jsp:include page="footer.jsp" flush="true"/>
