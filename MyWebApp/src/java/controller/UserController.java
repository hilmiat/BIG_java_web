/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.persistence.criteria.Predicate;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import static org.omg.IOP.CodecPackage.InvalidTypeForEncodingHelper.id;

/**
 *
 * @author my
 */
@WebServlet(name = "UserController", urlPatterns = {"/user"})
public class UserController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //halaman [list user,view user,add user]
        //baca data
//        request.setAttribute("datauser", model.DataUser.getData());
        



        //baca id yang akan di delete
        String id_delete = request.getParameter("delete");
        //hapus data dengan id tertentu
        
        if(id_delete!=null){
            new model.DataUser().deleteUser(Integer.parseInt(id_delete));
        }
        ArrayList<User> data = new model.DataUser().getData();
 
        request.setAttribute("datauser",data);
                
        String id = request.getParameter("id");
        if(id!=null){
            User x = new model.DataUser().getUserById(Integer.parseInt(id));
            request.setAttribute("user", x);
            request.setAttribute("hal", "viewUser.jsp");
                
        }else{
            request.setAttribute("hal", "listUser.jsp");
        }
        
        //jika request halaman add user
        String action = request.getParameter("action");
        if(action!=null && action.equals("add")){
            request.setAttribute("hal", "formUser.jsp");
        }else if(action!=null && action.equals("edit")){
            String idedit = request.getParameter("idedit");
            User useredit = null;
            if(idedit!=null){
                useredit = new model.DataUser().getUserById(Integer.parseInt(idedit));
            }
            
            request.setAttribute("user", useredit);
            request.setAttribute("hal", "formUser.jsp");
        }
        
        //forward ke halaman tampilan
        RequestDispatcher r = getServletContext().getRequestDispatcher("/index.jsp");
        r.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //baca data dari form
//        User data_form = (User) request.getAttribute("user");
//        PrintWriter pw = response.getWriter();
//        pw.print("username:"+request.getParameter("username"));
//        pw.print(",email:"+request.getParameter("email"));
//        pw.print(",address:"+request.getParameter("address"));
        
//        ArrayList<User> data = new model.DataUser().getData();
        User baru = new User(request.getParameter("username"),
                          "pass",
                          request.getParameter("email"), 
                          request.getParameter("address"), 
                          Integer.parseInt(request.getParameter("age"))
                        , 0);
        String idedit = request.getParameter("idedit");
        if(idedit!=null){
            //update data
            baru.setId(Long.parseLong(idedit));
            new model.DataUser().updateUser(baru);
        }else{
            new model.DataUser().insertUser(baru);
        }
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
