/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author my
 */
public class DataUser {
    Connection koneksi;
    
    void konekDB(){
        try {
            Class.forName("org.postgresql.Driver");
            this.koneksi = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/dataweb",
                    "hilmi","password");
        } catch (Exception ex) {
            
        }
    }
    public ArrayList<User> getData(){
        ArrayList<User> datauser = new ArrayList<>();
        //buat koneksi;
        this.konekDB();
        try{
            //prepare statement
            PreparedStatement st = 
                    koneksi.prepareStatement("SELECT * FROM tbl_user");
            //execute query
            ResultSet rs = st.executeQuery();
            //jika ada hasilnya,baca datanya
            while(rs.next()){
                datauser.add(
                        new User(rs.getString("username"), 
                                 rs.getString("password"),
                                 rs.getString("email"),
                                 rs.getString("address"),
                                 rs.getInt("age"),
                                 rs.getInt("id"))
                );
            }
        }catch(Exception ex){
            
        }
        return datauser;
    }
        
    public void insertUser(User userbaru){
        //buat koneksi
        this.konekDB();
        try{
        //siapkan statement
            PreparedStatement ps = 
                    this.koneksi.prepareStatement(
      "INSERT INTO tbl_user (username,email,password,address,age) values(?,?,?,?,?);"
                    );
        //siapkan data    
            ps.setString(1,userbaru.getUsername());
            ps.setString(2, userbaru.getEmail());
            ps.setString(3, userbaru.getPassword());
            ps.setString(4, userbaru.getAddress());
            ps.setInt(5, userbaru.getAge());
        //eksekusi data
            ps.executeUpdate();
            System.out.println("Sukses insert "+userbaru.getUsername());
        }catch(Exception ex){
            System.err.println(ex.getMessage());
        }
    }
//    
//    static ArrayList<User> data = new ArrayList<>();
//    
//    static{
//        data.add(new User("Adi", "password", "adi@gmail.com", "Cibinong", 28, 1));
//        data.add(new User("Adu", "password", "adu@gmail.com", "Bogor", 23, 2));
//        data.add(new User("Ade", "password", "ade@gmail.com", "Citeureup", 24, 3));
//        data.add(new User("Ado", "password", "ado@gmail.com", "Cisalak", 30, 4));
//        data.add(new User("Ada", "password", "ada@gmail.com", "Cisarua", 38, 5));
//    }
//    

    public User getUserById(int idcari) {
       //koneksi
       this.konekDB();
       //query
       String query = "SELECT * from tbl_user where id=?";
       try{
            PreparedStatement st = this.koneksi.prepareStatement(query);
            st.setInt(1, idcari);
            //get data
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                User usercari = new User();
                usercari.setId(rs.getInt("id"));
                usercari.setUsername(rs.getString("username"));
                usercari.setEmail(rs.getString("email"));
                usercari.setAddress(rs.getString("address"));
                usercari.setAge(rs.getInt("age"));
                usercari.setPassword(rs.getString("password"));
                return usercari;
            }else{
                System.err.println("data tidak ditemukan");
                return null;
            }
       }catch(Exception e){
           System.err.println("Gagal cari data");
           return null;
       }
    }

    public void updateUser(User baru) {
        this.konekDB();
        String sql = "UPDATE tbl_user SET username=?,email=?,"
                + "address=?,age=? where id=?";
        try{
            PreparedStatement ps = this.koneksi.prepareStatement(sql);
            ps.setString(1, baru.getUsername());
            ps.setString(2, baru.getEmail());
            ps.setString(3, baru.getAddress());
            ps.setInt(4, baru.getAge());
            ps.setInt(5, (int)baru.getId());
            ps.executeUpdate();
        }catch (Exception e){
            System.err.println("Gagal update "+e.getMessage());
        }
    }
    public void deleteUser(int iddelete){
        this.konekDB();
        String query="DELETE FROM tbl_user WHERE id=?";
        try{
            PreparedStatement ps = this.koneksi.prepareStatement(query);
            ps.setInt(1, iddelete);
            ps.executeUpdate();
        }catch(Exception e){
             System.err.println("Gagal delete "+e.getMessage());
        }
    }
   
}












