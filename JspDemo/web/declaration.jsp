<%-- 
    Document   : declaration
    Created on : Sep 6, 2016, 10:10:47 AM
    Author     : my
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    int x;
    Date theDate = new Date();
    Date getDate(){      
        return theDate;
    }
    Date getCurDate(){
        return new Date();
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h2>Date saat diinisialisasi <%=getDate()%></h2>
        <h2>Date saat ini <%=getCurDate()%></h2>
    </body>
</html>
