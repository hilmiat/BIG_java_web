<%-- 
    Document   : directive
    Created on : Sep 6, 2016, 9:49:19 AM
    Author     : my
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% int a=10; %>
        <h1>Demo directive include</h1>
        <hr/>
        <%--
            <%@include file="..." 
            akan meng-copy isi dari file tanpa mengeksekusi file tersebut
        --%>
            <%@include file="scriptlet.jsp" %>
        <hr/>
        <%=nama%>
    </body>
</html>
