<%-- 
    Document   : profile
    Created on : Sep 6, 2016, 1:42:43 PM
    Author     : my
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="user" class="user.UserData" scope="session"/>
<jsp:include page="header.jsp">
    <jsp:param name="judul" value="View Profile"></jsp:param>
</jsp:include>
    <body>
        <h1>Hello <%= user.getUsername() %></h1>
        <ul>
            <li>Email: <%= user.getEmail() %></li>
            <li>Address: <%= user.getAddress() %></li>
            <li>Age: <%= user.getAge() %></li>
        </ul>
        <%@taglib uri="/WEB-INF/tlds/demo.tld" prefix="demo"%>
        
        <demo:helo nama="Nama Saya"/>
    </body>
</html>
