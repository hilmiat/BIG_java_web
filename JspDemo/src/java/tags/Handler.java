/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tags;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 *
 * @author my
 */
public class Handler extends TagSupport{
    static final long serialVersionUID = 1L;
    private String nama;

    public void setNama(String nama) {
        this.nama = nama;
    }

    
    @Override
    public int doEndTag() throws JspException {
        try {
            //cetak hello...
            pageContext.getOut().print("HELLOO..."+this.nama);
        } catch (IOException ex) {
            throw new JspException("Gagal menulis");
        }
        return EVAL_PAGE;
    }
    
}
